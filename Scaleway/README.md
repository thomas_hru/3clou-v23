# Scaleway API CLI

## Instances

- **Create instance :**
~~~bash
scw instance server create \
    type=<instance type> \
    zone=<zone available> \
    image=<image uuid> \
    root-volume=local:<size>G \
    name=<instance name> ip=new \
    project-id=<project id>
~~~

- **Stop instance :**
~~~bash
scw instance server stop <instance id>
~~~

- **Delete instance :**
~~~bash
scw instance server delete <instance id>
~~~