
resource "scaleway_instance_ip" "thomash_srv1_ip" {}

resource "scaleway_instance_server" "thomash_srv1_apache2" {
    name  = "thomash_srv1_apache2"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "thomash", "apache2" ]
    ip_id = scaleway_instance_ip.thomash_srv1_ip.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv1.sh")
    }
  }

output "srv1_ip" {
  value = "${scaleway_instance_server.thomash_srv1_apache2.public_ip}"
}

