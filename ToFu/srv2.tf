
resource "scaleway_instance_ip" "thomash_srv2_ip" {}

resource "scaleway_instance_server" "thomash_srv2_nginx" {
    name  = "thomash_srv2_nginx"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "thomash", "nginx" ]
    ip_id = scaleway_instance_ip.thomash_srv2_ip.id
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_srv2.sh")
    }
}

output "srv2_ip" {
  value = "${scaleway_instance_server.thomash_srv2_nginx.public_ip}"
}